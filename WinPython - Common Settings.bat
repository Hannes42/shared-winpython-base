set PYTHONUSERBASE=Z:\WinPython

set THONNY_USER_DIR=Z:\WinPython\.thonny

if not exist Z:\WinPython\.thonny\configuration.ini (
    mkdir Z:\WinPython\.thonny
    mkdir Z:\WinPython\Scripts
    (
        echo [run]
        echo backend_configuration = Python (same as front-end^)
        echo working_directory = Z:\WinPython\Scripts
        echo [view]
        echo shellview.visible = True
        echo objectinspector.visible = True
        echo globalsview.visible = True
        echo name_highlighting = True
        echo locals_highlighting = True
        echo paren_highlighting = True
        echo syntax_coloring = True
        echo show_line_numbers = True
        echo recommended_line_length = 80
        echo [edit]
        echo tab_complete_in_editor = True
        echo tab_complete_in_shell = True
    ) > Z:\WinPython\.thonny\configuration.ini
)

if not exist Z:\WinPython\.jupyter\jupyter_notebook_config.py (
    mkdir Z:\WinPython\.jupyter
    (
        echo import webbrowser
        echo webbrowser.register('firefox', None, webbrowser.GenericBrowser('C:\\Program Files (x86^)\\Mozilla Firefox\\firefox.exe'^)^)
        echo c.NotebookApp.browser = 'firefox'
    ) > Z:\WinPython\.jupyter\jupyter_notebook_config.py
)

set PATH=%PYTHONUSERBASE%\Lib\site-packages\osgeo;%PYTHONUSERBASE%\Lib\site-packages\rtree;%PATH%

set GDAL_DATA=Z:\WinPython\Lib\site-packages\osgeo\data\gdal
set PROJ_LIB=Z:\WinPython\Lib\site-packages\osgeo\data\proj