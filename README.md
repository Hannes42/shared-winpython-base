# A shared, read-only WinPython for multiple users with persistent local settings and site-packages

TODO WinPython is meant to be used 
writable
persistent
installing separately for each user is 

- C: is a garbled mess between read-only, persistent and shared
- Y: is read-only
- Z: is persistent, user-specific storage (kind of like a home directory but not configured as such for the Windows host system)

## Goals
- A single WinPython installation/extraction, easily updated by us
- Integrated programming tools storing their temporary and configuration files in a persistent location in the user's control
- Packages being installed to a persistent location in the user's control

## Setup
- WinPython extracted to a read-only, accessible location such as a shared network drive. For us it is Y:\Misc\Programme\WPy-3661\
- We also installed Thonny using "pip install thonnyapp" in the WinPython Command Prompt as a user with write-permissions to the WinPython directory. That way, Thonny is available to all users.

### Common Settings

The following settings were bundled into a "common" .bat script that gets called by each launcher script before launching its program.

#### PYTHONUSERBASE
To be able to locally install packages to a persistent path within user control we can set PYTHONUSERBASE:

`set PYTHONUSERBASE=Z:\WinPython`

#### Thonny
##### THONNY_USER_DIR
Thonny stores and reads its configuration from a location that is volatile in our setup. To make it use a different directory, one can use the THONNY_USER_DIR environment variable:

`set THONNY_USER_DIR=Z:\WinPython\.thonny`

##### Interpreter
Thonny needs to be configured to use the WinPython interpreter instead of its own or an other one found on the system. When launched via the provided .bat file, Thonny's front-end will use the WinPython interpreter. We can tell it to use the same for its back-end.

The following .bat script, creates an initial configuration with that setting, provided no configuration exists yet.

```
if not exist Z:\WinPython\.thonny\configuration.ini (
    mkdir Z:\WinPython\.thonny
    (
        echo [run]
        echo backend_configuration = Python (same as front-end^)
    ) > Z:\WinPython\.thonny\configuration.ini
)
```

If the user wants to, they can change the configuration later on.

##### Default directory
`    mkdir Z:\WinPython\Scripts`
and add
`        echo working_directory = Z:\WinPython\Scripts`

##### Other stuff
```
        echo [view]
        echo shellview.visible = True
        echo objectinspector.visible = True
        echo globalsview.visible = True
        echo name_highlighting = True
        echo locals_highlighting = True
        echo paren_highlighting = True
        echo syntax_coloring = True
        echo show_line_numbers = True
        echo recommended_line_length = 80
        echo [edit]
        echo tab_complete_in_editor = True
        echo tab_complete_in_shell = True
```

#### Jupyter Notebook
##### Webbrowser to be opened
We want Jupyter Notebook to launch a Firefox instance instead of whatever non-sense might be the default browser on the system. This is really annoying to setup, here is how we solved it:

```

if not exist Z:\WinPython\.jupyter\jupyter_notebook_config.py (
    mkdir Z:\WinPython\.jupyter
    (
        echo import webbrowser
        echo webbrowser.register('firefox', None, webbrowser.GenericBrowser('C:\\Program Files (x86^)\\Mozilla Firefox\\firefox.exe'^)^)
        echo c.NotebookApp.browser = 'firefox'
    ) > Z:\WinPython\.jupyter\jupyter_notebook_config.py
)
```

If the user wants to, they can change the configuration later on.

#### Python Packages that come with DLLs
For user-installed Python packages that come with bundled DLLs we have to tell Windows where to find those. When used with `PYTHONUSERBASE` and `pip install --user package` those end up in `%PYTHONUSERBASE%\Lib\site-packages\package` or something similar (e.g. for GDAL it is very unintuitively `osgeo`...).

The user would need to copy and edit the .bat scripts to add their installed specialities. If you expect them to use this a lot, consider adding a check in the `Common Settings` script to look for user-supplied configuration in Z:, maybe they could store a text file with a list of paths there?

We know that some students will install GDAL and rtree (and they are supposed to do so on their own) so we added their DLLs to the path in advance:

`set PATH=%PYTHONUSERBASE%\Lib\site-packages\osgeo;%PYTHONUSERBASE%\Lib\site-packages\rtree;%PATH%`

Be aware that you probably won't be able to compile packages. Instead see if Gohlke's awesome [Unofficial Windows Binaries for Python Extension Packages](https://www.lfd.uci.edu/~gohlke/pythonlibs/) provide pre-built wheels for you. Install those via `pip install --user package.whl`. Keep in mind your version of Python and 32/64-bit when attempting this.

### Launchers
For each program that comes with WinPython first call the common settings script, then launch it. For example for Spyder:

```
call "WinPython - Common Settings.bat"
"Y:\Misc\Programme\WPy-3661\Spyder.exe"
```

## Usage
- You can launch the tools using the provided .bat scripts:
    - `WinPython - IDLE.bat`
    - `WinPython - IPython Qt Console.bat`
    - `WinPython - Jupyter Lab.bat`
    - `WinPython - Jupyter Notebook.bat`
    - `WinPython - Spyder.bat`
    - `WinPython - Spyder reset.bat`
    - `WinPython - Thonny.bat`
    - `WinPython - WinPython Command Prompt.bat`
    - `WinPython - WinPython Control Panel.bat`
    - `WinPython - WinPython Interpreter.bat`
    - `WinPython - WinPython Powershell Prompt.bat`
- You can install Python packages persistently with pip if you run `WinPython - WinPython Command Prompt.bat` and inside the shell that this opens `pip install --user package`
- Jupyter Notebooks save in Z:\WinPython\Notebooks by default
- Thonny saves in in Z:\WinPython\Scripts by default

## TODO
- Package installation via WinPython Control Panel is not working
- Pyzo?
- Spyder default save directory?
- IDLE default save directory?
- clean up Thonny section above...
- Spyder triggers Windows Defender at (first?) Launch
- get rid of windows confirmation dialogs at launching the bat scripts

## "References"
- https://stackoverflow.com/questions/15632663/launch-ipython-notebook-with-selected-browser
- https://stackoverflow.com/questions/19878136/how-can-i-use-a-batch-file-to-write-to-a-text-file
